<style>
  <?php if (!in_array('starting', $display)) { ?>
    .starting {display: none;}
  <?php } ?>

  <?php if (!in_array('completed', $display)) { ?>
    .completed {display: none;}
  <?php } ?>

  <?php if (!in_array('due', $display)) { ?>
    .due {display: none;}
  <?php } ?>

<?php if (!in_array('developers', $display)) { ?>
    .developers {display: none;}
  <?php } ?>

</style>


<p>
  <a href='?action=task' class='button'>New</a>
</p>




<form action='?action=dashboard' method='post'>
  <p>
    <label for='start'>Start</label>
    <input type='date' name='start' value='<?= $start->format("Y-m-d") ?>'>
  </p>

  <p>
    <label for='end'>End</label>
    <input type='date' name='end' value='<?= $end->format("Y-m-d") ?>'>
  </p>

  <p>
    <label>Developers</label>
    <?php foreach ($allDevelopers as $d) { ?>
    <label class='plain'>
      <input type='checkbox' name='developers[]' value='<?= $d ?>' checked>
      <?= $d ?>
    </label>
      
    <?php } ?>

  <p>
    <label>Display</label>
    <label class='plain'>
      <?php $checked = in_array('starting', $display) ? "checked" : ""; ?>
      <input type='checkbox' name='display[]' value='starting' <?= $checked ?>>
      Starting
    </label>

    <label class='plain'>
      <?php $checked = in_array('completed', $display) ? "checked" : ""; ?>
      <input type='checkbox' name='display[]' value='completed'  <?= $checked ?>>
      Completed
    </label>

    <label class='plain'>
      <?php $checked = in_array('due', $display) ? "checked" : ""; ?>
      <input type='checkbox' name='display[]' value='due'  <?= $checked ?>>
      Due
    </label>

    <label class='plain'>
      <?php $checked = in_array('developers', $display) ? "checked" : ""; ?>
      <input type='checkbox' name='display[]' value='developers'  <?= $checked ?>>
      Developers
    </label>

  </p>
  
  <p>
  <input type='submit' value='Update' class='button'>
  </p>
</form>

<table class='calendar'>
  <thead>
    <tr>
      <th>
        Sunday
      </th>
      <th>
        Monday
      </th>
      <th>
        Tuesday
      </th>
      <th>
        Wednesday
      </th>
      <th>
        Thursday
      </th>
      <th>
        Friday
      </th>
      <th>
        Saturday
      </th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <?php // first sunday and title td ?>
      <td class='title'>
        <div class='date'>
          &nbsp;
        </div>
        <div class='starting title'>
          <h3>
            Starting
          </h3>
        </div>

        <div class='completed title'>
          <h3>
            Completed
          </h3>
        </div>

        <div class='due title'>
          <h3>
            Due
          </h3>
        </div>

        <?php foreach ($developers as $d) { ?>
        <div class='developers title <?= $d ?>'>
          <h3>
            <?= $d ?>
          </h3>
        </div>
        <?php } ?>
      </td>
    
      <?php // remaining days of padding prior to start ?>
      <?php for($i = 1; $i < $start->format('w'); $i++) { ?>
        <td>
          <div class='date'> 
            &nbsp;
          </div>

          <div class='starting'>
            &nbsp;
          </div>

          <div class='completed'>
            &nbsp;
          </div>

          <div class='due'>
            &nbsp;
          </div>

          <?php foreach ($developers as $d) { ?>
            <div class='developers <?= $d ?>'>
              &nbsp;
            </div>
          <?php } ?>
        </td>
      <?php } ?>


      <?php // actual calendar dates requested ?>
      <?php for ($today = $start; $today <= $end; $today->modify("+1 day")) { ?>


        <?php // Sunday and title td ?>
        <?php // creates new tr ?>
        <?php if ($today->format('l') == "Sunday") { ?>
          </tr>
          <tr>

            <td class='title'>
              <div class='date title'>
                &nbsp;
              </div>
              <div class='starting title'>
                <h3>
                  Starting
                </h3>
              </div>

              <div class='completed title'>
                <h3>
                  Completed
                </h3>
              </div>

              <div class='due title'>
                <h3>
                  Due
                </h3>
              </div>

              <?php foreach ($developers as $d) { ?>
              <div class='developers title <?= $d ?>'>
                <h3>
                  <?= $d ?>
                </h3>
              </div>
              <?php } ?>
            </td>
            <?php continue; ?>
        <?php } ?>

        <?php // Remaining days of the week ?>
        <td>
          <div class='date'> 
            <?= $today->format('M j') ?>
          </div>

          <div class='starting'>
            <?php foreach ($calendar[$today->format('Y-m-d')]['starting'] as $t) { ?>
              <?= $t ?>
            <?php } ?>
          </div>

          <div class='completed'>


            <?php foreach ($calendar[$today->format('Y-m-d')]['completed'] as $t) { ?>
              <?= $t ?>
            <?php } ?>
          </div>

          <div class='due'>
            <?php foreach ($calendar[$today->format('Y-m-d')]['due'] as $t) { ?>
              <?= $t ?>
            <?php } ?>

          </div>


          <?php foreach ($developers as $d) { ?>
            <div class='developers <?= $d ?>'>
                
              <?php if (isset($calendar[$today->format('Y-m-d')]['burndown'][$d])) { ?>
                <?php foreach ($calendar[$today->format('Y-m-d')]['burndown'][$d] as $t) { ?>
                  <?= $t ?>
                <?php } ?>
              <?php } ?>

              
            </div>
          <?php } ?>
          
        </td>
      <?php } ?>

      <?php // Additional padding at the end of the month ?>
      <?php for($i = $start->format('w'); $i < 7 ; $i++) { ?>
         <td>
          <div class='date'> 
            &nbsp;
          </div>

          <div class='starting'>
            &nbsp;
          </div>

          <div class='completed'>
            &nbsp;
          </div>

          <div class='due'>
            &nbsp;
          </div>

          <?php foreach ($developers as $d) { ?>
            <div class='developers <?= $d ?>'>
              &nbsp;
            </div>
          <?php } ?>
        </td>
      <?php } ?>
    </tr>
  </tbody>

</table>
