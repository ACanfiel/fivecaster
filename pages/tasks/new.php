    <div id='taskNew'>
      <form action='index.php' method='POST'>
        <input type='hidden' name='action' value='taskSave'>

        <p>    
          <label for='name'>
            Name
          </label>
          <input type='text' name='name'>
        </p>
        
        <p>
        </p>

        <p>
          <label for='developer'>
            Developer
          </label>
          <select name='developer'>
            <?php foreach ($developers as $d) { ?>
            <option><?= $d?></option>
            <?php } ?>
          </select>
        </p>

        <p>
          <label for='priority'>
            Priority (1 low, 10 high)
          </label>
          <select name='priority'>
            <?php for ($i = 1; $i < 11; $i++) { ?>
            <?php $selected = ($i == 5) ? "selected" : ""; ?>
            <option <?= $selected ?>><?= $i?></option>
            <?php } ?>
          </select>
        </p>

        <p>
          <label for='start'>
            Start
          </label>
          <input type='date' name='start' value='<?= $today->format("Y-m-d") ?>'>
        </p>

        <p>
          <label for='due'>
            Due
          </label>
          <input type='date' name='due' value='<?= $today->modify("+1 week")->format("Y-m-d") ?>'>
        </p>

        <p>
          <label for='hours'>
            Hours
          </label>
          <input type='number' name='hours' min='0' max='100' value='0'>
        </p>

        <p>
          <label for='estimated'>
            Estimated
          </label>
          <input type='number' name='estimated' min='1' max='100' value='10'>
        </p>

         <p>
          <label for='link'>
            Link
          </label>
          <input type='text' name='link'>
        </p>

        <p>
        </p>

        <p>
          <input type='submit' value='Create' class='button'>
        </p>
      </form>
    </div>
