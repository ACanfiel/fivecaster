    <div id='taskEdit'>
      <p>
        <a href='?action=taskDelete&id=<?= $task->getId() ?>' class='button'>Delete</a>
      </p>

      <form action='index.php' method='POST'>
        <input type='hidden' name='action' value='taskSave'>

        <p>      
          <label for='name'>
            Name
          </label>
          <input type='text' name='name' value='<?= $task->getName() ?>' >
        </p>

        <p>      
          <label for='id'>
            Id
          </label>
          <input type='text' name='id' value='<?= $task->getId() ?>' readonly >
        </p>

        <p>
          <label for='developer'>
            Developer
          </label>
          <select name='developer'>
            <?php foreach ($developers as $d) { ?>
              <?php $selected = ($d == $task->getDeveloper()) ? 'selected' : ''; ?>
            <option <?= $selected ?>><?= $d?></option>
            <?php } ?>
          </select>
        </p>
        
        <p>
          <label for='priority'>
            Priority (1 low, 10 high)
          </label>
          <select name='priority'>
            <?php for ($i = 1; $i < 11; $i++) { ?>
            <?php $selected = ($d == $task->getPriority()) ? 'selected' : ''; ?>
            <option $selected><?= $i?></option>
            <?php } ?>
          </select>
        </p>
        
        <p>
          <label for='start'>
            Start
          </label>
          <input type='date' name='start' value='<?= $task->getStart() ?>'>        
        </p>
        
        <p>
          <label for='due'>
            Due
          </label>
          <input type='date' name='due' value='<?= $task->getDue() ?>'>
        </p>
        
        <p>
          <label for='hours'>
            Hours
          </label>
          <input type='number' name='hours' min='0' max='100' value='<?= $task->getHours() ?>'>
        </p>
        
        <p>
          <label for='estimated'>
            Estimated
          </label>
          <input type='number' name='estimated' min='1' max='100' value='<?= $task->getEstimated() ?>'>
        </p>

        <p>
          <label for='link'  style='margin-bottom: 10px'>
            <a href='<?= $task->getLink() ?>' class='button' target="_new">Agile JIRA</a>
          </label>
          <input type='text' name='link' value='<?= $task->getLink() ?>'>
        </p>

        <p>
        </p>

        
        <p>
          <input type='submit' value='Update' class='button'>
        </p>
      </form>
    </div>
