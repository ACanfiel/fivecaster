<?php


class Tasks {
  public static $hoursPerDay = 7;

  public static $developers = array(
    "Andrew",
    "Bo",
    "Parun",
    "Rashmi",
    "Sankar",
  );

  public static $columns = array(
    'Id',
    'Name',
    'Developer',
    'Priority',

    'Start',
    'Due',

    'Hours',
    'Estimated',
    'Completed',
  );


  public static function allocate($start, $end, array $tasks, array $developers) {
    $response = array();
    $burndown = array();
      foreach ($developers as $d)
        $burndown[$d] = array();
    $processed = array();


    $start = new DateTime($start);
    $end = new DateTime($end);

    // Prior incomplete tasks
    while (count($tasks) && $tasks[0]->getStart() <= $start->format("Y-m-d")) {
      $task = array_shift($tasks);
      
      $burndown[$task->getDeveloper()][] = $task;
      $processed[] = $task;
    }
    

    // Process tasks
    for ($current = $start; $current <= $end; $current->modify("+1 day")) { 
      $today = $current->format('Y-m-d');

      $response[$today] = array(
        'starting' => array(),
        'completed' => array(),
        'due' => array(),
      );

      // Load today's tasks
      while (count($tasks) && $tasks[0]->getStart() <= $today) {
        $task = array_shift($tasks);

        $response[$today]['starting'][] = clone $task;

        $burndown[$task->getDeveloper()][] = $task;
        $processed[] = $task;
      }


      // process burndown list
      foreach ($developers as $d) {
        if ($burndown[$d]) { 
          usort($burndown[$d], "Tasks::sortByPriority");
        }

        // skip weekends
        if ($current->format('l') !== "Saturday" && $current->format('l') !== "Sunday") {
          for ($i = 0; $i < Tasks::$hoursPerDay; $i++) {
            if (count($burndown[$d]) > 0) { 
              if ($burndown[$d][0]->isComplete()) { 
                $task = array_shift($burndown[$d]);
                $response[$today]['completed'][] = clone $task;
              }
              
              if (count($burndown[$d]) > 0) { 
                $burndown[$d][0]->logWork("1");
              }
            }
          }
        }

        

        // update response
        foreach ($burndown[$d] as $t) {
          $response[$today]['burndown'][$d][] = clone $t;
        }
      }

      // Check for due
      foreach($processed as $t) {
        if ($t->getDue() == $today) {
          $response[$today]['due'][] = clone $t;
        }
      }
    }


    return $response;
  }

  public static function sortByPriority($a, $b) {
    if ($a->getPriority() == $b->getPriority()) { 
      if ($a->getId() > $b->getId())
        return 1;
      else
        return -1;
    }

    if ($a->getPriority() > $b->getPriority())
      return -1;

    return 1;
  }

  public static function find($db, $conditions = null, $order = null) {
    $sql  = "SELECT * FROM tasks" . PHP_EOL;

    if ($conditions)
      $sql .= "WHERE $conditions" . PHP_EOL;
    if ($order)
      $sql .= "ORDER BY $order" . PHP_EOL; 

    $result = $db->query($sql);

    $data = array();
    while ($result && $r = $result->fetch_assoc()) 
      $data[] = new Task($r['id'], $r['name'], $r['developer'], $r['priority'], $r['start'], $r['due'], $r['hours'], $r['estimated'], $r['completed'], $r['link']);

    return $data;
  }

  public static function findOne($db, $conditions = null, $order = null) {
    return Tasks::find($db, $conditions, $order)[0];
  }

  public static function save($db, Task $task) {
    if ($task->getId()) { 
      return Tasks::update($db, $task);
    }

    return Tasks::insert($db, $task);
  }

  public static function insert($db, Task $task) {
    $sql  = "INSERT INTO tasks (id, name, developer, priority, start, due, hours, estimated, completed, link)" . PHP_EOL;
    $sql .= "  VALUES" . PHP_EOL;
    $sql .= "('" . $task->getId() . "', '" . $task->getName() . "', '" . $task->getDeveloper(). "', '" .$task->getPriority(). "', '" .$task->getStart(). "', '" .$task->getDue(). "', '" .$task->getHours(). "', '" .$task->getEstimated() . "', 'false', '" . $task->getLink() . "')";

    $result = $db->query($sql);
  }

  public static function update($db, Task $task) {
    $sql  = "UPDATE tasks" . PHP_EOL;
    $sql .= "SET " . PHP_EOL;

    $elements = array();
    foreach (Tasks::$columns as $c) {
      $method = "get" . $c;
      $elements[] = strtolower($c) . "='" . $task->$method() . "'";
    }
    
    $sql .= join(", ", $elements);
    $sql .= " WHERE id='" . $task->getId() . "'" . PHP_EOL;

    echo $sql;

    $result = $db->query($sql);
  }

  public static function delete($db, $conditions = null) {
    $sql = "DELETE FROM tasks" . PHP_EOL;
    if ($conditions)
      $sql .= "  WHERE $conditions" . PHP_EOL;

    //echo $sql;

    $result = $db->query($sql);
  }

  public static function createDatabase($db) {
    $sql = "CREATE DATABASE tasks";

    $result = $db->query($sql);
  }

  public static function createTable($db) {
    $sql  = "CREATE TABLE tasks (" . PHP_EOL;
    $sql .= "  id INT AUTO_INCREMENT," . PHP_EOL;
    $sql .= "  name VARCHAR(255) NOT NULL," . PHP_EOL;
    $sql .= "  developer VARCHAR(255) NOT NULL," . PHP_EOL;
    $sql .= "  priority INT NOT NULL," . PHP_EOL;
    $sql .= "  start DATE," . PHP_EOL;
    $sql .= "  due DATE," . PHP_EOL;
    $sql .= "  hours INT," . PHP_EOL;
    $sql .= "  estimated INT," . PHP_EOL;
    $sql .= "  completed BOOLEAN," . PHP_EOL;
    $sql .= "  link VARCHAR(255)," . PHP_EOL;
    $sql .= "  PRIMARY KEY(id)" . PHP_EOL;
    $sql .= ")" . PHP_EOL;

    $result = $db->query($sql);
  }

  public static function dropTable($db) {
    $sql = "DROP TABLE tasks" . PHP_EOL;

    $result = $db->query($sql);
  }


  public static function createRandom($db, $count = 10) {
    $tasks = array();

    for ($i = 0; $i < $count; $i++) {
      $startOffset = mt_rand(0, 60);
      $dueOffset = $startOffset + mt_rand(0, 30);

      $name = "Task $i"; 
      $developer = Tasks::$developers[rand(0, count(Tasks::$developers) - 1)];
      $priority = rand(1, 9);
      $start = date('Y-m-d', strtotime('+' . $startOffset . ' days'));
      $due = date('Y-m-d', strtotime('+' . $dueOffset . ' days'));
      $estimated = rand(1, 100);

      $sql  = "INSERT INTO tasks (name, developer, priority, start, due, hours, estimated, completed)" . PHP_EOL;
      $sql .= "VALUES('$name', '$developer', '$priority', '$start', '$due', '0', '$estimated', 'false')" . PHP_EOL;

      
      $result = $db->query($sql);
    }
  }

  public static function importCSV($db, $filename) {
    echo "<pre>";

    $file = fopen($filename, "r");
    
    while (!feof($file)) {
      $today = new DateTime();

      $row = fgetcsv($file);
 
      $project = $row[0] . "-";

      $id = str_replace($project, "", $row[1]);
      $name = $row[2];
      
      
      $developer = "";
      foreach (Tasks::$developers as $d) {
        if (strpos($row[7], $d) !== false) {
          $developer = $d;
        }
      }
      
      $priority = 5;

      $start = $today->format('Y-m-d');
      echo $row[17];
      if ($row[17] != "") {
        $date = new DateTime($row[17]);
        $due = $date->format('Y-m-d');
      } else {
        $due = "";
      }


      $hours = (int)$row[22] / 60 / 60;
      $estimated = (int)$row[20] / 60 / 60;

      $link = "https://agile-jira.ciena.com/browse/" . $row[1];

      $task = new Task($id, $name, $developer, $priority, $start, $due, $hours, $estimated, false, $link);
      Tasks::delete($db, "id='" . $task->getId() . "'");
      Tasks::insert($db, $task);
    }
  }
}