<?php

class Task {
  private $id;
  private $name;
  private $developer;
  private $priority;

  private $start;
  private $due;

  private $estimated;
  private $hours;
  private $completed;

  private $link;

  public function __construct($id, $name, $developer, $priority, $start, $due, $hours, $estimated, $completed = false, $link = null) {
    $this->setId($id);
    $this->setName($name);
    $this->setDeveloper($developer);
    $this->setPriority($priority);
    $this->setStart($start);
    $this->setDue($due);
    $this->setHours($hours);
    $this->setEstimated($estimated);
    $this->setCompleted($completed);
    $this->setLink($link);
  }



  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function getDeveloper() {
    return $this->developer;
  }

  public function setDeveloper($developer) {
    $this->developer = $developer;
  }

  public function getPriority() {
    return $this->priority;
  }

  // Tasks are priority 1(low) - 10 (high)
  // Meetings are priority 11
  public function setPriority($priority) {
    if (!is_int((int)$priority))
      throw new Exception("Priority must be an integer.");

    if ($priority < 1 && $priority > 11)
      throw new Exception("Priority out of range.");

    $this->priority = $priority;
  }

  public function getStart() {
    return $this->start;
  }

  public function setStart($date) {
    if (!is_date($date))
      throw new Exception("Start must be a date");

    $this->start = $date;
  }

  public function getDue() {
    return $this->due;
  }

  public function setDue($date) {
    if (!is_date($date))
      throw new Exception("Due must be a date");

    $this->due = $date;
  }

  public function getEstimated() {
    return $this->estimated;
  }

  public function setEstimated($hours) {
    if (!is_int((int)$hours))
      throw new Exception("Estimated must be an integer.");

    if ($hours < 1 && $hours > 100)
      throw new Exception("Estimated out of range.");

    $this->estimated = $hours;
  }

  public function getHours() {
    return $this->hours;
  }

  public function setHours($hours) {
    if (!is_int((int)$hours))
      throw new Exception("Hours must be an integer.");

    if ($hours < 0 && $hours > 100)
      throw new Exception("Hours out of range.");

    $this->hours = $hours;
  }

  public function isComplete() {
    return $this->completed;
  }

  public function getCompleted() {
    return $this->completed;
  }

  public function setCompleted($bool) {
    $this->completed = $bool;
  }

  public function getLink() {
    return $this->link;
  }

  public function setLink($link) {
    $this->link = $link;
  }



  public function logWork($hours) {
    $this->hours += $hours;
    if ($this->hours >= $this->estimated)
      $this->completed = true;
  }

  

  public function __toString() {
    $response = "<div class='task'>";
    $response .= "<span class='priority'>[{$this->priority}] </span>";
    $response .= "<span class='name'><a href='?action=taskEdit&id={$this->id}'>" . substr($this->name, 0, 30) . "</a> ({$this->hours} / {$this->estimated})";
    $response .= "</div>";

    return $response;
  }

}
