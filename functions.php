<?php

function fetch($view, $data = null) {
  if (!is_file($view))
    throw new Exception("$view does not exist");

  if ($data)
    extract($data);

  ob_start();
  require $view;
  return ob_get_clean();
}

function render($view, $data = null) {
  if (!is_file($view))
    throw new Exception("$view does not exist");

  if ($data)
    extract($data);

  require $view;
}


function getAction() {
  return isset($_REQUEST['action']) ? $_REQUEST['action'] : 'dashboard';
}

function getValue(array $method, $key, array $allowed = null) {
  if (!isset($method[$key]))
    return null;

  $value = $method[$key];

  if (isset($allowed)) {
    if (!is_array($value)) { 
      if (!in_array($value, $allowed)) { 
        return null;
      }
    } else {
      foreach ($value as $v) {
        if (!in_array($v, $allowed)) {
          return null;
        }
      }
    }
  }

  return $value;
}

// TODO: date validation
function is_date($date) {
  return true;
}

function redirect($location = null) {
  if ($location)
    header("Location: " . $location);
  else
    header("Location: " . $_SERVER['PHP_SELF']);
  die();
}