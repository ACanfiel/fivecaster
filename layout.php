<!DOCTYPE html>
<html>
  <head>
    <title>FiveCaster</title>

    <link rel='stylesheet' type='text/css' href='styles/styles.css'>    

  </head>
<body>
  <div class='container'>
    <h1>FiveCaster</h1>
  </div>

  <nav class='container'>
    <ul>
      <li>
        <a href='?action=dashboard'>
          Dashboard
        </a>
      </li>
      <li>
        <a href='?action=tasks'>
          Tasks
        </a>
      </li>
      <li>
        <a href='?action=admin'>
          Admin
        </a>
      </li>
    </ul>
  </nav>

  <div class='container'>
    <div id='content'>
      <?= isset($content) ? $content : null; ?>
    </div>

  </div>
    
</body>
</html>