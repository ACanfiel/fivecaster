</pre>
<table>
  <thead>
    <tr>
    
    <?php foreach ($columns as $column) { ?>
      <th>
        <?= $column ?>
      </th>
    <?php } ?>

      <th>
      </th>
    </tr>
  </thead>

  <tbody>
    <?php foreach ($items as $item) { ?>
    <tr>

      <?php foreach ($columns as $column) { ?>
      <td>
        <?php $method = "get" . $column; ?>
        <?= substr($item->$method(), 0, 25) ?>
      </td>
      <?php } ?>

      <td>
        <?php foreach ($actions as $action => $link) { ?>
          <a href='<?= $link . $item->getId() ?>' class='button'><?= $action ?></a>
        <?php } ?>
      </td>
    </tr>
    <?php } ?>

  </tbody>
</table>
