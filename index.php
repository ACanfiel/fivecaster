<?php

  // CHANGE ME SEYMOUR!!
  $db = new mysqli('localhost', 'root', 'OneTwo3!', 'tasks');



  require_once('functions.php');
  require_once('classes/Task.php');
  require_once('classes/Tasks.php');
  

  $action = getAction();


  switch($action) {
    // Servable pages
    case 'dashboard':
      

      $today = new DateTime();
      
      $startString = getValue($_POST, 'start');
      if (is_null($startString))
        $startString = $today->format("Y-m-d");
      $start = new DateTime($startString);

      $endString = getValue($_POST, 'end');
      if (is_null($endString))
        $endString = $today->modify("+1 month")->format("Y-m-d");
      $end = new DateTime($endString);
      
      $developers = getValue($_POST, 'developers', Tasks::$developers);
      if (is_null($developers))
        $developers = Tasks::$developers;


      $display = getValue($_POST, 'display');
      if (is_null($display))
        $display = array(
          'starting', 
          'completed',
          'due',
          'developers',
        );

      $tasks = Tasks::find($db, 'completed=false', 'start, priority');
      $calendar = Tasks::allocate($start->format('Y-m-d'), $end->format('Y-m-d'), $tasks, $developers);


      $content = fetch('pages/dashboard.php', array(
        'start' => $start,
        'end' => $end,
        'allDevelopers' => Tasks::$developers,
        'developers' => $developers,
        'display' => $display,
        'calendar' => $calendar,
      ));

      render('layout.php', array(
        'content' => $content,
      ));

    break;

    case 'admin':
      $content = fetch('pages/admin.php');

      render('layout.php', array(
        'content' => $content,
      ));

    break;

    case 'tasks':
      $tasks = Tasks::find($db, null, "id");
      $table = fetch('partials/table.php', array(
        'columns' => Tasks::$columns,
        'items' => $tasks,
        'actions' => array(
          'Edit' => '?action=taskEdit&id=',
          'Delete' => '?action=taskDelete&id=',
        ),
      ));

      $content = fetch('pages/tasks/list.php', array(
        'table' => $table
      ));

      render('layout.php', array(
        'content' => $content
      ));
    break;

    case 'task':
      $content = fetch('pages/tasks/new.php', array(
        'today' => new DateTime(),
        'developers' => Tasks::$developers,
      ));

      render('layout.php', array(
        'content' => $content,
      ));
    break;

    case 'taskEdit':
      $id = (int)getValue($_GET, 'id');

      if (!is_int($id))
        throw new Exception("Invalid id: $id");

      $task = Tasks::findOne($db, "id='$id'");

      $content = fetch('pages/tasks/edit.php', array(
        'developers' => Tasks::$developers,
        'task' => $task,
      ));

      render('layout.php', array(
        'content' => $content,
      ));
    break;


    // Functionality
    case 'createTables':
      Tasks::createDatabase($db);
      Tasks::createTable($db);
      redirect();
    break;

    case 'reset':
      Tasks::delete($db);
      Tasks::dropTable($db);
      Tasks::createTable($db);
      redirect();
    break;

    case 'createRandomTasks':
      Tasks::createRandom($db);
      redirect();
    break;

    case 'taskSave':
      $id = getValue($_POST, 'id');
      $name = getValue($_POST, 'name');
      $developer = getValue($_POST, 'developer', Tasks::$developers);
      $priority = (int)getValue($_POST, 'priority');
      $start = getValue($_POST, 'start');
      $due = getValue($_POST, 'due');
      $hours = (int)getValue($_POST, 'hours');
      $estimated = (int)getValue($_POST, 'estimated');

      $task = new Task($id, $name, $developer, $priority, $start, $due, $hours, $estimated);
      Tasks::save($db, $task);
      redirect();
    break;

    case 'taskDelete':
      $id = (int)getValue($_GET, 'id');
      if (!is_int($id))
        throw new Exception("Invalid id");

      Tasks::delete($db, "id='$id'");
      redirect('?action=tasks');
    break;

    case 'importCSV':
      print_r($_FILES);


      if (isset($_FILES['csv'])) { 
        $filename = 'uploads/' . $_FILES['csv']['name'];

        if (!move_uploaded_file($_FILES['csv']['tmp_name'], $filename)) {
          print_r($_FILES);
          die("Upload failed, try a smaller file.");
        } 

        Tasks::importCSV($db, $filename);
      }

      redirect('?action=tasks');
    break;

    default:
      redirect();
  }
